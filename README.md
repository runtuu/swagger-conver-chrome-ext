# swagger-conver-chrome-ext

#### 介绍

Chrome 插件：Swagger.json 可视化编辑并导出其他格式文档（Word/Markdown）

#### 安装

- 插件商店安装：[点我](https://chrome.google.com/webstore/detail/swagger%E8%BD%AC%E6%8D%A2%E5%B7%A5%E5%85%B7/iiljplhneagfmmfeimgijpmkbiabgakd)

- 导入插件文件安装: 地址栏中打开`chrome://extensions`，把项目目录下的`swagger-convert.crx`文件，[点我下载](https://gitee.com/sdsxfjh/swagger-conver-chrome-ext/raw/master/swagger-convert.crx)直接拖放到页面上即可。
  - 如果提示`该扩展程序未列在 Chrome 网上应用店中，并可能是在您不知情的情况下添加的`，可以按照如下步骤解决：
  - 1、首先把需要安装的第三方插件，后缀.crx 改成 .rar，然后解压，得到一个文件夹
  - 2、再打开`chrome://extensions/`谷歌扩展应用管理，点击右上角的开发者模式，就可以看到“加载正在开发的扩展程序”这一选项。
  - 3、选择刚才步骤 1 中解压好的文件夹，确定
  - 4、确认新增扩展程序，点击添加，成功添加应用程序。
- 手动编译安装：下载代码，安装依赖，运行`npm run build`，打开 chrome 插件开发者模式，加载`/dist`目录即可。

#### 本地调试

```
npm i
npm run watch:dev
```

打开浏览器，选择`更多工具`->`扩展程序`，或者地址栏输入`chrome://extensions`，打开右上角`开发者模式`，选择`加载已解压的扩展程序`，选择项目根目录下的`dist`目录即可。

> 一点说明：工具属于初级版本，使用过程中可能异常暴毙，可能接口不太规范，再加上工具代码健壮性比较差 🐶，请及时联系我，如果能提供给我当时`Swagger.json`文件，那就更好了 😁。

#### 使用

现阶段只有导出和接口筛选功能，`格式定制`、`分组筛选`、`接口分类`、`导出其他格式文档`等等功能还没来的及做，只能导出 html，然后直接复制到 Word 中，我做了一些 Word 中字体和格式的适配，其他功能等有空了吧...

![第一步](https://images.fengjiaheng.top/images/chromeExt/20190922/001.png)

![第二步](https://images.fengjiaheng.top/images/chromeExt/20190922/002.png)

![第三步](https://images.fengjiaheng.top/images/chromeExt/20190922/003.png)

![第四步](https://images.fengjiaheng.top/images/chromeExt/20190922/004.png)
