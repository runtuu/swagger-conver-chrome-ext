// import store from './store';
global.browser = require('webextension-polyfill');
// console.log(store);

chrome.browserAction.onClicked.addListener(function(tab) {
  chrome.tabs.create({ url: chrome.runtime.getURL('index/index.html') }, function(tab) {
    // 暂时搁置一个优化：只打开一个唯一的标签页
  });
});

function loadFile() {
  const url = chrome.runtime.getURL('data/swagger.json');
  let result = {};
  fetch(url)
    .then(response => response.json()) // assuming file contains json
    .then(json => {
      result = json;
    });
  return result;
}

function saveFile() {
  console.log('saveFile');
}
