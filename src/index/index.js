import Vue from 'vue';
import App from './App';
import iView from 'iview';
import 'iview/dist/styles/iview.css';

global.browser = require('webextension-polyfill');

Vue.use(iView);

/* eslint-disable no-new */
new Vue({
  el: '#app',
  render: h => h(App),
});
