module.exports = {
  oData: [
    {
      title: '获取三维瓦片数据结果',
      schema: 'deleteFeatures',
      input: [
        {
          parame: 'datasourceName',
          description: '场景名',
          type: 'String',
        },
        {
          parame: 'datasetName',
          description: '图层名',
          type: 'String',
        },
        {
          parame: 'ids',
          description: '瓦片参数',
          type: 'String',
        },
      ],
      output: [
        {
          parame: 'RealspaceDataResult',
          description: '瓦片数据结果',
          type: 'Array',
        },
      ],
    },
    {
      title: '获取三维瓦片数据结果',
      schema: 'deleteFeatures',
      input: [
        {
          parame: 'datasourceName',
          description: '场景名',
          type: 'String',
        },
        {
          parame: 'datasetName',
          description: '图层名',
          type: 'String',
        },
        {
          parame: 'ids',
          description: '瓦片参数',
          type: 'String',
        },
      ],
      output: [
        {
          parame: 'RealspaceDataResult',
          description: '瓦片数据结果',
          type: 'Array',
        },
      ],
    },
    {
      title: '获取三维瓦片数据结果',
      schema: 'deleteFeatures',
      input: [
        {
          parame: 'datasourceName',
          description: '场景名',
          type: 'String',
        },
        {
          parame: 'datasetName',
          description: '图层名',
          type: 'String',
        },
        {
          parame: 'ids',
          description: '瓦片参数',
          type: 'String',
        },
      ],
      output: [
        {
          parame: 'RealspaceDataResult',
          description: '瓦片数据结果',
          type: 'Array',
        },
      ],
    },
    {
      title: '获取三维瓦片数据结果',
      schema: 'deleteFeatures',
      input: [
        {
          parame: 'datasourceName',
          description: '场景名',
          type: 'String',
        },
        {
          parame: 'datasetName',
          description: '图层名',
          type: 'String',
        },
        {
          parame: 'ids',
          description: '瓦片参数',
          type: 'String',
        },
      ],
      output: [
        {
          parame: 'RealspaceDataResult',
          description: '瓦片数据结果',
          type: 'Array',
        },
      ],
    },
    {
      title: '获取三维瓦片数据结果',
      schema: 'deleteFeatures',
      input: [
        {
          parame: 'datasourceName',
          description: '场景名',
          type: 'String',
        },
        {
          parame: 'datasetName',
          description: '图层名',
          type: 'String',
        },
        {
          parame: 'ids',
          description: '瓦片参数',
          type: 'String',
        },
      ],
      output: [
        {
          parame: 'RealspaceDataResult',
          description: '瓦片数据结果',
          type: 'Array',
        },
      ],
    },
  ],
};
